Simple program to help me add root keys to hosts

I put together this little script (and web pages) that allows one to run the following little curl to add all the users keys that are in github to authorized_keys.  

Run like so

`curl -s -L https://tinyurl.com/add-git-keys | bash /dev/stdin <client>`

example

`curl -s -L https://tinyurl.com/add-git-keys | bash /dev/stdin mibr`

You can also just check it out and run it from the command line.

You can run with gitlab keys as well by using the `--gitlab` flag. Uses github otherwise.

`curl -s -L https://tinyurl.com/add-git-keys | bash /dev/stdin <client> --gitlab`

In salt you can create a pillar

`techsquare: client: eaps file:`

then run

`salt-call state.sls add-git-keys`

The repo is here

`git@gitlab.com:techsquareinc/add-git-keys.git`

TODO:

    - add arguments 

    - --user= - add to users keys instead of root

    - add some security to http://mirror.techsquare.com/.sb (currently only obscurity)
