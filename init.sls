add-keys-{{ pillar['techsquare']['client'] }}:
  cmd.run:
    - name: "curl -s -L https://tinyurl.com/add-git-keys | bash /dev/stdin  {{ pillar['techsquare']['client'] }} {{ pillar['techsquare']['file'] }} && touch /root/.ts.{{ pillar['techsquare']['client'] }}"
    - creates: /root/.ts.{{ pillar['techsquare']['client'] }}

